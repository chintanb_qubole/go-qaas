package deployercommands

import (
  "fmt"
  "utils"
  "log"
)
func deployStatusImpl(environment string) {
  fmt.Println("Reached impl of deploy status with params:",environment)
  
  command := fmt.Sprintf("kubectl --namespace %s get deployments pods svc statefulsets"
  err, out, errout := utils.Runcmd(command)
  if err != nil {
      log.Printf("error: %v\n", err)
  }
  
  // fmt.Println("--- stdout ---")
  // fmt.Println(out)
  // fmt.Println("--- stderr ---")
  // fmt.Println(errout)
  
}

func connectPodImpl(environment string,pod_name string) {
  fmt.Println("Reached impl of connect pod with params:",environment,pod_name)
  command := fmt.Sprintf("kubectl --namespace %s exec -it %s bash",environment,deployment_name)
  err, out, errout := utils.Runcmd(command)
  if err != nil {
      log.Printf("error: %v\n", err)
  }

}