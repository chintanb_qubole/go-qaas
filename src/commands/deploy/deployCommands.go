package deployercommands

import (
	"github.com/galdor/go-cmdline"
  "fmt"
)

func CommandsDef(commands **cmdline.CmdLine) {
  (**commands).AddCommand("deploy","status")
  (**commands).AddCommand("deploy","destroy")  
}
func DeployCommand(args []string) {
  fmt.Println("Hello")
  // fmt.Printf("running command \"deploy\" with arguments %v\n", args)
  
  switch args[1] {
    case "status":
      deployStatus(args[1:])
    case "destroy":
      deployDestroy(args[1:])  
    }
}

func deployStatus(args []string) {
  fmt.Println("called deploy:status")
  cmdline := cmdline.New()
	cmdline.AddOption("e", "environment", "value", "Name of enviroment to deploy on")
  cmdline.AddOption("", "release-version", "value", "Name of version to deploy on")
	cmdline.Parse(args)

	if cmdline.IsOptionSet("e") {
		fmt.Println("environment:", cmdline.OptionValue("e"))
	}
	deployStatusImpl(cmdline.OptionValue("e"))
  // if cmdline.IsOptionSet("release-version") {
  //   fmt.Println("release-version: %s\n", cmdline.OptionValue("release-version"))
  // }
}

func deployDestroy(args []string) {
  fmt.Println("called deploy:destroy")
  cmdline := cmdline.New()
	cmdline.AddOption("e", "environment", "value", "Name of enviroment to deploy on")
  cmdline.Parse(args)

	if cmdline.IsOptionSet("e") {
		fmt.Println("environment:", cmdline.OptionValue("e"))
	}
}
