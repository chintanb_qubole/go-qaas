package connectcommands

import (
	"github.com/galdor/go-cmdline"
  "fmt"
)

func CommandsDef(commands **cmdline.CmdLine) {
  (**commands).AddCommand("connect","deployment")
  (**commands).AddCommand("connect","pod")  
}

func ConnectCommand(args []string) {  
  switch args[1] {
  case "deployment":
      connectDeployment(args[1:])
    case "statefulset":
      connectStatefulset(args[1:])
    case "daemonset":
      connectDaemonset(args[1:]) 
    case "pod":
      connectPod(args[1:])
    }
}

func baseConnectCommandCli(cmdline **cmdline.CmdLine) {
  
  (**cmdline).AddOption("e", "environment", "value", "Name of enviroment to deploy on")
  (**cmdline).AddOption("n", "name", "name", "Name of deployment to connect to")
}

func printCmdParams(cmdline **cmdline.CmdLine, args []string){
  (**cmdline).Parse(args)
  if (**cmdline).IsOptionSet("e") {
    fmt.Println("environment:", (**cmdline).OptionValue("e"))
  }
  if (**cmdline).IsOptionSet("name") {
    fmt.Println("name:", (**cmdline).OptionValue("name"))
  }
}

func connectDeployment(args []string) {
  cmdline := cmdline.New()
  baseConnectCommandCli(&cmdline)
  printCmdParams(&cmdline,args)
  environment := (cmdline).OptionValue("environment")
  deployment_name := (cmdline).OptionValue("name")
  connectDeploymentImpl(environment,deployment_name)
}

func connectPod(args []string) {
  cmdline := cmdline.New()
  baseConnectCommandCli(&cmdline)
  printCmdParams(&cmdline,args)
}

func connectDaemonset(args []string) {
  cmdline := cmdline.New()
  baseConnectCommandCli(&cmdline)
  printCmdParams(&cmdline,args)
}

func connectStatefulset(args []string) {
  cmdline := cmdline.New()
  baseConnectCommandCli(&cmdline)
  printCmdParams(&cmdline,args)
}