package connectcommands
import (
  "fmt"
  "utils"
  "log"
)
func connectDeploymentImpl(environment string, deployment_name string) {
  fmt.Println("Reached impl of connect deployment with params:",environment,deployment_name)
  
  command := fmt.Sprintf("kubectl --namespace %s get -o template %s/%s --template={{.metadata.labels}}",environment,"deployment",deployment_name)
  err, out, errout := utils.Runcmd(command)
  if err != nil {
      log.Printf("error: %v\n", err)
  }
  
  // fmt.Println("--- stdout ---")
  // fmt.Println(out)
  // fmt.Println("--- stderr ---")
  // fmt.Println(errout)
  
}

func connectPodImpl(environment string,pod_name string) {
  fmt.Println("Reached impl of connect pod with params:",environment,pod_name)
  command := fmt.Sprintf("kubectl --namespace %s exec -it %s bash",environment,deployment_name)
  err, out, errout := utils.Runcmd(command)
  if err != nil {
      log.Printf("error: %v\n", err)
  }

}
