package main

import (
	"os"
	"github.com/galdor/go-cmdline"
  "fmt"
  "commands/deploy"
  "commands/connect"
)

func main() {

  cmdline := cmdline.New()
  deployercommands.CommandsDef(&cmdline)
  connectcommands.CommandsDef(&cmdline)
  
  cmdline.Parse(os.Args)
  fmt.Println(cmdline.CommandName())
  switch cmdline.CommandName() {
	case "deploy":  
	 	deployercommands.DeployCommand(cmdline.CommandNameAndArguments())
	case "connect":
    connectcommands.ConnectCommand(cmdline.CommandNameAndArguments())
	 }
   
}
